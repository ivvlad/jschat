import { ICreateMessage, IMessage } from '../services/interfaces';

export const validateMessage = (message: ICreateMessage): boolean => {
  const { userId, user, text, createdAt } = message;

  if (userId && user && text && createdAt) {
    return true;
  }

  return false;
};

export const createMessageBody = (message: ICreateMessage): IMessage => ({
  id: message.id,
  userId: message.userId,
  avatar: message.avatar,
  user: message.user,
  text: message.text,
  createdAt: message.createdAt,
  editedAt: new Date().toISOString(),
  likes: message.likes || [],
});
