import { v4 } from 'uuid';

import { INewUser, IRegister, IReturnedUser } from '../services/interfaces';

export const createUser = (
  data: IRegister,
  currentBody = {} as INewUser
): INewUser | boolean => {
  try {
    if (currentBody?.username) {
      const newUser = {
        username: data.username || currentBody?.username,
        password: data.password || currentBody?.password,
        userId: currentBody?.userId || v4(),
        userData: {
          avatar: data.avatar || currentBody?.userData.avatar || '',
          userName: data.name || currentBody?.userData.userName || 'Unknown',
          isAdmin: data.isAdmin || false,
        },
      };

      return newUser;
    }

    const newUser = {
      username: data.username,
      password: data.password,
      userId: v4(),
      userData: {
        avatar: data.avatar || '',
        userName: data.name || 'Unknown',
        isAdmin: data.isAdmin || false,
      },
    };

    return newUser;
  } catch {
    return false;
  }
};

export const getUserData = (user: INewUser): IReturnedUser => ({
  userId: user.userId,
  userName: user.userData.userName,
  avatar: user.userData.avatar,
  isAdmin: user.userData.isAdmin,
});
