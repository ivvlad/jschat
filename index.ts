import express from 'express';
import cors from 'cors';

import routes from './routes/index';

const app = express();
const port = 3002;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

routes(app);

app.use('/', express.static('./client/build'));

app.listen(port, () => {});

export default app;