import { Response } from 'express';
import { IMessagesService, IMessage } from './interfaces';
import messagesRepository from '../repositories/messageRepository';
import { validateMessage, createMessageBody } from '../helpers/messages';

class MessagesService implements IMessagesService {
  getMessages(res: Response) {
    const messages = messagesRepository.getAll();

    if (messages) {
      return res.json({ messages });
    }

    return res.status(400).json({ error: true, message: 'Bad request' });
  }

  createMessage(message: IMessage, res: Response) {
    const isValidate = validateMessage(message);

    if (isValidate) {
      const body = createMessageBody(message);
      const status = messagesRepository.create(body);

      if (status) {
        return res.json({ status });
      }
    }

    return res.status(400).json({ error: true, message: 'Bad request' });
  }

  updateMessage(messageId: string, message: IMessage, res: Response) {
    const isExist = messagesRepository.getOne(messageId, false);

    if (isExist) {
      const body = createMessageBody(message);
      const status = messagesRepository.update(messageId, body);

      if (status) {
        return res.json({ status });
      }

      return res.status(400).json({ error: true, message: 'Bad request' });
    }

    return res.status(404).json({ error: true, message: 'Message not found' });
  }

  deleteMessage(messageId: string, res: Response) {
    const isExist = messagesRepository.getOne(messageId, false);

    if (isExist) {
      const id = messagesRepository.delete(messageId);

      if (id) {
        return res.json({ id });
      }

      return res.status(400).json({ error: true, message: 'Bad request' });
    }

    return res.status(404).json({ error: true, message: 'Message not found' });
  }
}

export default new MessagesService();
