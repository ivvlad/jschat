import { Response } from 'express';

export interface IUserData {
  avatar: string;
  userName: string;
  isAdmin: boolean;
}

export interface IRegister {
  username: string;
  password: string;
  name?: string;
  avatar?: string;
  isAdmin?: false;
}

export interface INewUser {
  username: string;
  password: string;
  userId: string;
  userData: IUserData;
}

export interface IReturnedUser {
  userId: string;
  avatar: string;
  userName: string;
  isAdmin: boolean;
}

export interface ILogin {
  username: string;
  password: string;
}

export interface IAuthService {
  login(userData: ILogin, res: Response): Response;
}

export interface IMessagesService {
  getMessages(res: Response): Response;
}

export interface IMessage {
  id: string;
  userId: string;
  avatar: string;
  user: string;
  text: string;
  createdAt: string;
  editedAt: string;
  likes: string[];
}

export interface ICreateMessage {
  id: string;
  userId: string;
  avatar: string;
  user: string;
  text: string;
  createdAt: string;
  likes?: string[];
}
