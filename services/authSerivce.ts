import { Response } from 'express';
import { ILogin, IAuthService } from './interfaces';
import userService from './userService';

class AuthSevice implements IAuthService {
  login(userData: ILogin, res: Response): Response {
    const user = userService.search(userData);

    if (!user) {
      return res.status(404).json({ error: true, message: 'User not found' });
    }

    return res.json({ ...(user as object) }); //eslint-disable-line
  }
}

export default new AuthSevice();
