import { Request, Response } from 'express';
import userRepository from '../repositories/userRepository';
import {
  ILogin,
  IUserData,
  INewUser,
  IReturnedUser,
  IRegister,
} from './interfaces';
import { createUser, getUserData } from '../helpers/user';

class UserService {
  search(data: ILogin): IUserData | boolean {
    const { username, password } = data;
    const user = userRepository.getOne(username, true);

    if (user && user.password === password) {
      return getUserData(user);
    }

    return false;
  }

  getUsers(res: Response) {
    const users = userRepository.getAll();

    if (users) {
      return res.json({
        users: users.map((user: INewUser): IReturnedUser => getUserData(user)),
      });
    }

    return res.status(400).json({ error: true, message: 'Bad request' });
  }

  createUser(req: Request, res: Response): Response {
    const body = createUser(req.body);

    if (body) {
      const status = userRepository.create(body);

      if (status) {
        return res.json({ body: getUserData(body as INewUser) });
      }
    }

    return res.status(400).json({ message: 'Bad request' });
  }

  updateUser(userId: string, userBody: IRegister, res: Response) {
    const currentBody = userRepository.getOne(userId, false, true);

    if (currentBody) {
      const body = createUser(userBody, currentBody);
      const status = userRepository.update(userId, body as INewUser);

      if (status) {
        return res.json({ body: getUserData(body as INewUser) });
      }

      return res.status(400).json({ error: true, message: 'Bad request' });
    }

    return res.status(404).json({ error: true, message: 'Message not found' });
  }

  deleteUser(userId: string, res: Response) {
    const isExist = userRepository.getOne(userId, false, true);

    if (isExist) {
      const id = userRepository.delete(userId);

      if (id) {
        return res.json({ id });
      }

      return res.status(400).json({ error: true, message: 'Bad request' });
    }

    return res.status(404).json({ error: true, message: 'Message not found' });
  }
}

export default new UserService();
