/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, Link } from 'react-router-dom';
import moment from 'moment';

import styles from './header.module.css';
import defaultAvatar from '../../assets/userImage.png';
import searchIcon from './assets/search.svg';
import exitIcon from './assets/exit.svg';

import { IRootState } from '../../store/reducer';
import { logout } from '../../state/auth/auth.actions';
import { getUsersCount } from '../../helpers/chat';

// eslint-disable-next-line
const Header: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const { messages, chatName, userName, avatar, isAdmin } = useSelector(
    (state: IRootState) => ({
      messages: state.chat.messages,
      chatName: state.chat.chatName,
      userName: state.auth.userName,
      avatar: state.auth.avatar,
      isAdmin: state.auth.isAdmin,
    })
  );

  const [menuActive, setMenuActive] = useState(false);

  const usersCount: number = getUsersCount(messages);
  const lastMessageTime =
    (messages.length && messages[messages.length - 1].createdAt) || '';
  const formattedDate = `${moment(lastMessageTime).format('DD.MM.YYYY HH:mm')}`;

  return (
    <>
      <header className={`${styles.header} header`}>
        <div className={styles.menuWrapper}>
          <div className={styles.burgerWrapper}>
            <button
              className={styles.menuBurger}
              type="button"
              onClick={() => setMenuActive(!menuActive)}
            >
              <span className="visually-hidden">Меню</span>
            </button>
          </div>
        </div>

        <div className={styles.chatInfoWrapper}>
          <h3 className={`${styles.chatName} header-title`}>{chatName}</h3>
          <div className={styles.chatInfo}>
            <div>
              <p className="header-users-count">{usersCount}</p>
              <p className={styles.chatSeparator}>|</p>
              <p className="header-messages-count">{messages.length}</p>
            </div>
            <p className="header-last-message-date">{formattedDate}</p>
          </div>
        </div>

        <div className={styles.accountWrapper}>
          <button className={styles.searchIcon} type="button">
            <img src={searchIcon} alt="Поиск" />
          </button>
          <button
            className={styles.settingsButton}
            type="button"
            onClick={() => {
              dispatch(logout());
              history.push('/login');
            }}
          >
            <img src={exitIcon} alt="Выход" />
          </button>
          <a href="/#">
            <span className="visually-hidden">Account</span>
            <img
              className={styles.userAvatar}
              src={avatar || defaultAvatar}
              alt={`Аватар пользователя ${userName}`}
            />
          </a>
        </div>
      </header>
      <nav className={`${styles.menu} ${menuActive ? styles.menuVisible : ''}`}>
        <ul className={styles.linkWrapper}>
          <li onClick={() => setMenuActive(!menuActive)}>
            <Link className={styles.link} to="/">
              Chat
            </Link>
          </li>
          {isAdmin && (
            <li onClick={() => setMenuActive(!menuActive)}>
              <Link className={styles.link} to="/user-list">
                User List
              </Link>
            </li>
          )}
        </ul>
      </nav>
    </>
  );
};

export default Header;
