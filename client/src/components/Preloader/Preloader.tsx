import React from 'react';
import { IPreloader } from '../../interfaces';

import styles from './preloader.module.css';

const Preloader: React.FC<IPreloader> = ({ visibility }) => (
  <div
    className={`${
      visibility ? styles.loadingWrapper : styles.nonVisible
    } preloader`}
  >
    <div className={styles.loading}>
      <p>loading</p>
      <span />
    </div>
  </div>
);

export default Preloader;
