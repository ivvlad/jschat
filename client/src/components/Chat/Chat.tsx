/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import styles from './chat.module.css';
import ChatBody from './ChatBody/ChatBody';
import ChatInput from './ChatInput/ChatInput';

import { IChatContainerProps } from '../../interfaces';
import { getMessages } from '../../state/chat/chat.actions';
import { IRootState } from '../../store/reducer';

const Chat: React.FC<IChatContainerProps> = () => {
  const dispatch = useDispatch();

  const apiUrl = process.env.REACT_APP_API_URL;

  const { messages } = useSelector((state: IRootState) => ({
    messages: state.chat.messages,
  }));

  useEffect(() => {
    if (!messages.length) {
      dispatch(getMessages(`${apiUrl}/messages/`));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={`${styles.chatWrapper} chat`}>
      <ChatBody />
      <ChatInput />
    </div>
  );
};

export default Chat;
