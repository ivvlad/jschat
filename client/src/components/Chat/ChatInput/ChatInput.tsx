import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import style from './chatInput.module.css';

import { uniqueId } from '../../../helpers/chat';
import {
  setEditMode,
  setEditMessageId,
  handleAddMessage,
} from '../../../state/chat/chat.actions';
import { IRootState } from '../../../store/reducer';

const ChatInput: React.FC = () => {
  const dispatch = useDispatch();

  const apiUrl = process.env.REACT_APP_API_URL;

  const { messages, userId, avatar, userName } = useSelector(
    (state: IRootState) => ({
      messages: state.chat.messages,
      userId: state.auth.userId,
      avatar: state.auth.avatar,
      userName: state.auth.userName,
    })
  );

  const [text, setText] = useState('');

  const handleSendMessage = (): void => {
    if (text) {
      dispatch(
        handleAddMessage(
          `${apiUrl}/messages/new`,
          text,
          uniqueId(),
          userId,
          avatar,
          userName
        )
      );
      setText('');
    }
  };

  const handleonKeyDown = (ev: React.KeyboardEvent): void => {
    if (ev.code === 'ArrowUp') {
      const userMessages = messages.filter(
        (message) => message.userId === userId
      );
      const lastUserMessage = userMessages[userMessages.length - 1];

      if (lastUserMessage) {
        dispatch(setEditMessageId(lastUserMessage.id));
        dispatch(setEditMode(true));
      }
    }
  };

  return (
    <div className={`${style.inputWrapper} message-input`}>
      <textarea
        className={`${style.textArea} message-input-text`}
        value={text}
        onChange={(ev: React.FormEvent<HTMLTextAreaElement>) =>
          setText(ev.currentTarget.value)
        }
        onKeyDown={handleonKeyDown}
      />
      <button
        className={`${style.submitButton} message-input-button`}
        type="button"
        onClick={handleSendMessage}
      >
        Отправить
      </button>
    </div>
  );
};

export default ChatInput;
