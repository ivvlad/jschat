import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import styles from './userEditor.module.css';

import { IRootState } from '../../../store/reducer';
import { IUser } from '../../../interfaces';
import {
  setChangeUser,
  createUser,
  handleChangeUser,
} from '../../../state/auth/auth.actions';
import { ICreateUser } from '../../../state/auth/auth.interfaces';

const UserEditor: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const apiUrl = process.env.REACT_APP_API_URL;

  const { users, changedUserId } = useSelector((state: IRootState) => ({
    users: state.auth.users,
    changedUserId: state.auth.changedUserId,
  }));

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');
  const [url, setUrl] = useState('');
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    if (changedUserId) {
      const user = users.find((user: IUser) => user.userId === changedUserId);

      if (user) {
        setName(user.userName || '');
        setUrl(user.avatar || '');
        setChecked(user.isAdmin || false);
      }
    }
  }, [changedUserId]); //eslint-disable-line

  const handleSumbit = () => {
    const data: ICreateUser = {
      username: username.replaceAll(' ', ''),
      password,
      name,
      avatar: url,
      isAdmin: checked,
    };

    if (!changedUserId && username.length > 3 && password.length > 3) {
      dispatch(createUser(`${apiUrl}/users/new`, data));
      dispatch(setChangeUser(''));
      history.push('/user-list');

      return;
    }

    dispatch(handleChangeUser(`${apiUrl}/users/${changedUserId}`, data));
    dispatch(setChangeUser(''));
    history.push('/user-list');

    return;
  };

  return (
    <div>
      <div className={styles.wrapper}>
        <div className={styles.form}>
          <p>Имя пользователя</p>
          <input
            type="text"
            value={username}
            onChange={(ev) => setUsername(ev.currentTarget.value)}
          />
          <p>Пароль</p>
          <input
            type="password"
            value={password}
            onChange={(ev) => setPassword(ev.currentTarget.value)}
          />
          <p>Имя</p>
          <input
            type="text"
            value={name}
            onChange={(ev) => setName(ev.currentTarget.value)}
          />
          <p>Аватар(url)</p>
          <input
            type="url"
            value={url}
            onChange={(ev) => setUrl(ev.currentTarget.value)}
          />
          <div className={styles.checkboxWrapper}>
            <input
              type="checkbox"
              checked={checked}
              onChange={() => setChecked(!checked)}
            />
            <p>Администратор</p>
          </div>
          <button
            className={styles.button}
            type="button"
            onClick={handleSumbit}
          >
            Сохранить
          </button>
        </div>
      </div>
    </div>
  );
};

export default UserEditor;
