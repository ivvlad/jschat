import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import styles from './usersList.module.css';
import User from './User/User';

import { getUsers } from '../../../state/auth/auth.actions';
import { IRootState } from '../../../store/reducer';

const UsersList: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const apiUrl = process.env.REACT_APP_API_URL;

  const { users, isAdmin } = useSelector((state: IRootState) => ({
    users: state.auth.users,
    isAdmin: state.auth.isAdmin,
  }));

  if (!isAdmin) {
    history.push('/');
  }

  useEffect(() => {
    if (!users.length) {
      dispatch(getUsers(`${apiUrl}/users`));
    }
  }, []); //eslint-disable-line

  const handleCreateUser = () => {
    history.push('/user-editor');
  };

  return (
    <div className={styles.wrapper}>
      <button
        className={styles.button}
        type="button"
        onClick={handleCreateUser}
      >
        Добавить пользователя
      </button>
      <div className={styles.usersWrapper}>
        <ul className={styles.usersList}>
          {users.map((user) => (
            <User key={user.userId} user={user} />
          ))}
        </ul>
      </div>
    </div>
  );
};

export default UsersList;
