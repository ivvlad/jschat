import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import styles from './user.module.css';
import userImage from '../../../../assets/userImage.png';

import { IUserContainer } from '../../../../interfaces';
import {
  setChangeUser,
  handleDeleteUser,
} from '../../../../state/auth/auth.actions';
import { IRootState } from '../../../../store/reducer';

const User: React.FC<IUserContainer> = ({ user }) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const { userId } = useSelector((state: IRootState) => ({
    userId: state.auth.userId,
  }));

  const apiUrl = process.env.REACT_APP_API_URL;

  const handleChange = useCallback(() => {
    dispatch(setChangeUser(user.userId));
    history.push('/user-editor');
  }, [history]); //eslint-disable-line

  const handleDelete = () => {
    dispatch(handleDeleteUser(`${apiUrl}/users/${user.userId}`));
  };

  return (
    <li className={styles.user}>
      <img
        className={styles.avatar}
        src={user.avatar || userImage}
        alt={`Автар пользователя ${user.userName}`}
      />
      <h4 className={styles.name}>{user.userName}</h4>
      <div>
        <button className={styles.button} type="button" onClick={handleChange}>
          Изменить
        </button>
        <button
          className={styles.button}
          type="button"
          onClick={handleDelete}
          disabled={user.userId === userId}
        >
          Удалить
        </button>
      </div>
    </li>
  );
};

export default User;
