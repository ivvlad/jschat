import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Switch, useHistory } from 'react-router-dom';

import { IRootState } from '../store/reducer';

import Login from './Login/Login';
import Chat from './Chat/Chat';
import Header from './Header/Header';
import UsersList from './Users/UsersList/UsersList';
import UserEditor from './Users/UserEdtior/UserEditor';

const Routes = () => {
  const hitsory = useHistory();

  const { userId } = useSelector((state: IRootState) => ({
    userId: state.auth.userId,
  }));

  if (!userId) {
    hitsory.push('/login');
  }

  return (
    <Switch>
      <Route exact path="/login" component={Login} />
      <>
        <Header />
        <Route exact path="/" component={Chat} />
        <Route exact path="/user-list" component={UsersList} />
        <Route exact path="/user-editor" component={UserEditor} />
      </>
    </Switch>
  );
};

export default Routes;
