import React, { useState } from 'react';

import styles from './login.module.css';
import { useDispatch, useSelector } from 'react-redux';
import { login } from '../../state/auth/auth.actions';
import { IRootState } from '../../store/reducer';
import { useHistory } from 'react-router-dom';

const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const apiUrl = process.env.REACT_APP_API_URL;

  const { userId } = useSelector((state: IRootState) => ({
    userId: state.auth.userId,
  }));

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = (): void => {
    if (username && password) {
      dispatch(login(`${apiUrl}/auth/login`, { username, password }, history));
    }
  };

  const handleKeyDown = (ev: any): void => {
    if (ev.key === 'Enter') {
      handleSubmit();
    }
  };

  if (userId) {
    history.push('/');
  }

  return (
    <div className={styles.wrapper} onKeyDown={handleKeyDown}>
      <article className={styles.loginForm}>
        <div className={styles.inputWrapper}>
          <label htmlFor="username">
            Имя пользователя
            <br />
            <input
              id="username"
              type="text"
              value={username}
              onChange={(ev) => setUsername(ev.currentTarget.value)}
            />
          </label>
          <label htmlFor="password">
            Пароль
            <br />
            <input
              id="password"
              type="password"
              value={password}
              onChange={(ev) => setPassword(ev.currentTarget.value)}
            />
          </label>
        </div>
        <button className={styles.button} type="button" onClick={handleSubmit}>
          Войти
        </button>
      </article>
    </div>
  );
};

export default Login;
