export interface IChatContainerProps {
  url: string;
}

export interface IHeader {
  user: IUser;
  userCount: number;
  messagesCount: number;
  lastMessageTime: string;
}

export interface IMessageDateContainer {
  date: string;
  messages: IMessage[];
}

export interface IMessageContainer {
  key: string;
  message: IMessage;
}

export interface IUserContainer {
  user: IUser;
}

export interface IPreloader {
  visibility: boolean;
}

export interface IMessageResponse {
  id: string;
  userId: string;
  avatar: string;
  user: string;
  text: string;
  createdAt: string;
  editedAt: string;
}

export interface IMessage {
  id: string;
  userId: string;
  avatar: string;
  user: string;
  text: string;
  createdAt: string;
  editedAt: string;
  likes: string[];
}

export interface IUser {
  userId: string;
  avatar: string;
  userName: string;
  isAdmin?: boolean;
}
