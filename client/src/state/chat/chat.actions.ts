import { Dispatch } from 'redux';
import {
  SET_MESSAGES,
  ADD_MESSAGE,
  SET_PRELOADER,
  EDIT_MESSAGE,
  SET_EDIT_MESSAGE_ID,
  SET_EDIT_MODE,
  DELETE_MESSAGE,
} from './chat.types';
import { IMessage, IMessageResponse, IUser } from '../../interfaces';
import { UPDATE_MESSAGE } from './chat.types';
import { IUpdateMessage } from './chat.interfaces';
import {
  ISetMessages,
  IEditMessage,
  IAddMessage,
  IDeleteMessage,
  IPreloaderAction,
  IEditMode,
  IEditMessageId,
} from './chat.interfaces';

export const setMessages = (payload: IMessage[]): ISetMessages => ({
  type: SET_MESSAGES,
  payload,
});

export const setEditMessage = (payload: IMessage[]): IEditMessage => ({
  type: EDIT_MESSAGE,
  payload,
});

export const addMessage = (payload: IMessage): IAddMessage => ({
  type: ADD_MESSAGE,
  payload,
});

export const updateMessage = (payload: IMessage): IUpdateMessage => ({
  type: UPDATE_MESSAGE,
  payload,
});

export const deleteMessage = (payload: string): IDeleteMessage => ({
  type: DELETE_MESSAGE,
  payload,
});

export const setPreloader = (payload: boolean): IPreloaderAction => ({
  type: SET_PRELOADER,
  payload,
});

export const setEditMode = (payload: boolean): IEditMode => ({
  type: SET_EDIT_MODE,
  payload,
});

export const setEditMessageId = (payload: string): IEditMessageId => ({
  type: SET_EDIT_MESSAGE_ID,
  payload,
});

export const getMessages = (url: string) => async (dispatch: Dispatch) => {
  dispatch(setPreloader(true));

  const response = await fetch(url, {
    cache: 'no-cache',
  });

  if (response.ok) {
    const json = await response.json();
    const updatedMessages: IMessage[] = json.messages.map(
      (messageData: IMessageResponse) => ({ ...messageData, likes: [] })
    );

    dispatch(setMessages(updatedMessages));
  } else {
    console.warn('Message request error.');
  }

  dispatch(setPreloader(false));
};

export const handleAddMessage =
  (
    url: string,
    text: string,
    id: string,
    userId: string,
    avatar: string,
    userName: string
  ) =>
  async (dispatch: Dispatch) => {
    dispatch(setPreloader(true));

    const message: IMessage = {
      id: id,
      userId: userId || '',
      avatar: avatar || '',
      user: userName || '',
      text,
      createdAt: new Date().toISOString(),
      editedAt: '',
      likes: [],
    };

    const response = await fetch(url, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify(message),
    });

    if (response.ok) {
      const json = await response.json();

      if (json.status) {
        dispatch(addMessage(message));
      }
    } else {
      console.warn('Message request error.');
    }

    dispatch(setPreloader(false));
  };

export const handleUpdateMessage =
  (
    url: string,
    messageId: string,
    text: string,
    user: IUser,
    message: IMessage
  ) =>
  async (dispatch: Dispatch) => {
    dispatch(setPreloader(true));

    const updatedMessage: IMessage = {
      id: messageId,
      userId: user.userId,
      avatar: user.avatar,
      user: user.userName,
      text,
      createdAt: message.createdAt,
      editedAt: new Date().toISOString(),
      likes: message.likes,
    };

    const response = await fetch(url, {
      method: 'PUT',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify(updatedMessage),
    });

    if (response.ok) {
      const json = await response.json();

      if (json.status) {
        dispatch(updateMessage(updatedMessage));
      }
    }

    dispatch(setPreloader(false));
  };

export const handleDeleteMessage =
  (url: string) => async (dispatch: Dispatch) => {
    dispatch(setPreloader(true));

    const response = await fetch(url, {
      method: 'DELETE',
      cache: 'no-cache',
    });

    if (response.ok) {
      const json = await response.json();

      dispatch(deleteMessage(json.id));
    }

    dispatch(setPreloader(false));
  };
