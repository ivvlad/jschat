import { IChatState, IChatAction } from './chat.interfaces';
import { IMessage } from '../../interfaces';
import {
  SET_MESSAGES,
  ADD_MESSAGE,
  SET_PRELOADER,
  UPDATE_MESSAGE,
  SET_EDIT_MESSAGE_ID,
  SET_EDIT_MODE,
  DELETE_MESSAGE,
} from './chat.types';

const initialState: IChatState = {
  messages: [],
  editModal: false,
  preloader: true,
  editMessageId: '',
  chatName: 'Binary',
};

// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initialState, action: IChatAction): IChatState => {
  switch (action.type) {
    case SET_MESSAGES:
      return { ...state, messages: action.payload };
    case ADD_MESSAGE:
      return { ...state, messages: [...state.messages, action.payload] };
    case UPDATE_MESSAGE:
      return {
        ...state,
        messages: state.messages.map((message): IMessage => {
          if (message.id === action.payload.id) {
            return action.payload;
          }

          return message;
        }),
      };
    case SET_PRELOADER:
      return { ...state, preloader: action.payload };
    case SET_EDIT_MODE:
      return { ...state, editModal: action.payload };
    case SET_EDIT_MESSAGE_ID:
      return { ...state, editMessageId: action.payload };
    case DELETE_MESSAGE:
      return {
        ...state,
        messages: state.messages.filter(
          (message) => message.id !== action.payload
        ),
      };
    default:
      return state;
  }
};
