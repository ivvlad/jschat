import { IAuthState, IAuthAction } from './auth.interfaces';

import userImage from '../../assets/userImage.png';
import { ADD_USER, CHANGE_USER } from './auth.types';
import {
  SET_USER,
  LOGOUT,
  SET_USERS,
  SET_CHANGE,
  DELETE_USER,
} from './auth.types';

const initialState: IAuthState = {
  userId: '',
  avatar: userImage,
  userName: '',
  isAdmin: false,
  changedUserId: '',
  users: [],
};

// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initialState, action: IAuthAction): IAuthState => {
  switch (action.type) {
    case SET_USER:
      return { ...state, ...action.payload };
    case SET_USERS:
      return { ...state, users: [...action.payload] };
    case ADD_USER:
      return { ...state, users: [...state.users, action.payload] };
    case CHANGE_USER:
      return {
        ...state,
        users: [
          ...state.users.filter(
            (user) => user.userId !== action.payload.userId
          ),
          action.payload,
        ],
      };
    case SET_CHANGE:
      return { ...state, changedUserId: action.payload };
    case DELETE_USER:
      return {
        ...state,
        users: state.users.filter((user) => user.userId !== action.payload),
      };
    case LOGOUT:
      return { ...action.payload };
    default:
      return state;
  }
};
