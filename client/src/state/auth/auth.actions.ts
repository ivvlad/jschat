import { Dispatch } from 'redux';

import {
  SET_USER,
  LOGOUT,
  SET_USERS,
  SET_CHANGE,
  DELETE_USER,
  ADD_USER,
} from './auth.types';
import {
  IAuthState,
  ISetUser,
  ILoginBody,
  ILogout,
  ISetUsers,
} from './auth.interfaces';

import userImage from '../../assets/userImage.png';
import { IUser } from '../../interfaces';
import { CHANGE_USER } from './auth.types';
import { IChangeUser } from './auth.interfaces';
import {
  ISetChangeUser,
  ICreateUser,
  IDeleteUser,
  IAddUser,
} from './auth.interfaces';

const setUserData = (payload: IAuthState): ISetUser => ({
  type: SET_USER,
  payload,
});

export const logout = (): ILogout => ({
  type: LOGOUT,
  payload: {
    userId: '',
    userName: '',
    avatar: userImage,
    isAdmin: false,
    changedUserId: '',
    users: [],
  },
});

const setUsers = (payload: IUser[]): ISetUsers => ({
  type: SET_USERS,
  payload,
});

const deleteUser = (payload: string): IDeleteUser => ({
  type: DELETE_USER,
  payload,
});

const addUser = (payload: IUser): IAddUser => ({
  type: ADD_USER,
  payload,
});

const changeUser = (payload: IUser): IChangeUser => ({
  type: CHANGE_USER,
  payload,
});

export const setChangeUser = (payload: string): ISetChangeUser => ({
  type: SET_CHANGE,
  payload,
});

export const login =
  (url: string, body: ILoginBody, history: any) =>
  async (dispatch: Dispatch<ISetUser>) => {
    const response = await fetch(url, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify(body),
    });

    if (response.ok) {
      const json = await response.json();

      dispatch(setUserData(json));

      if (json.isAdmin) {
        history.push('/user-list');
      } else {
        history.push('/');
      }
    }
  };

export const getUsers =
  (url: string) => async (dispatch: Dispatch<ISetUsers>) => {
    const response = await fetch(url, {
      cache: 'no-cache',
    });

    if (response.ok) {
      const json = await response.json();

      dispatch(setUsers(json.users));
    }
  };

export const createUser =
  (url: string, data: ICreateUser) => async (dispatch: Dispatch<IAddUser>) => {
    const response = await fetch(url, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify(data),
    });

    if (response.ok) {
      const json = await response.json();

      dispatch(addUser(json.body));
    }
  };

export const handleChangeUser =
  (url: string, data: ICreateUser) =>
  async (dispatch: Dispatch<IChangeUser>) => {
    const response = await fetch(url, {
      method: 'PUT',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify(data),
    });

    if (response.ok) {
      const json = await response.json();

      dispatch(changeUser(json.body));
    }
  };

export const handleDeleteUser = (url: string) => async (dispatch: Dispatch) => {
  const response = await fetch(url, {
    method: 'DELETE',
    cache: 'no-cache',
  });

  if (response.ok) {
    const json = await response.json();

    dispatch(deleteUser(json.id));
  }
};
