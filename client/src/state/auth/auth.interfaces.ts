import { IUser } from '../../interfaces';
export interface IAuthState {
  userId: string;
  avatar: string;
  userName: string;
  isAdmin: boolean;
  changedUserId: string;
  users: IUser[];
}

export interface IAuthAction {
  type: string;
  payload: any; // eslint-disable-line
}

export interface ISetUser {
  type: string;
  payload: IAuthState;
}

export interface ILoginBody {
  username: string;
  password: string;
}

export interface ILogout {
  type: string;
  payload: IAuthState;
}

export interface ISetUsers {
  type: string;
  payload: IUser[];
}

export interface ISetChangeUser {
  type: string;
  payload: string;
}

export interface IDeleteUser {
  type: string;
  payload: string;
}

export interface IAddUser {
  type: string;
  payload: IUser;
}

export interface IChangeUser {
  type: string;
  payload: IUser;
}

export interface ICreateUser {
  username: string;
  password: string;
  name?: string;
  avatar?: string;
  isAdmin?: boolean;
}
