export const SET_USER = 'SET_USER';
export const SET_USERS = 'SET_USERS';
export const ADD_USER = 'ADD_USER';
export const CHANGE_USER = 'CHANGE_USER';
export const SET_CHANGE = 'SET_CHANGE';
export const DELETE_USER = 'DELETE_USER';
export const LOGOUT = 'LOGOUT';
