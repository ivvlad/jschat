import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import styles from './message.module.css';
import closeIcon from './assets/close.svg';

import { IRootState } from '../../store/reducer';
import { IMessage, IUser } from '../../interfaces';
import {
  setEditMessageId,
  setEditMode,
  handleUpdateMessage,
} from '../../state/chat/chat.actions';

const MessageEdit: React.FC = () => {
  const dispatch = useDispatch();

  const apiUrl = process.env.REACT_APP_API_URL;

  const {
    messages,
    editModal,
    editMessageId,
    userId,
    avatar,
    userName,
    isAdmin,
  } = useSelector((state: IRootState) => ({
    messages: state.chat.messages,
    editModal: state.chat.editModal,
    editMessageId: state.chat.editMessageId,
    userId: state.auth.userId,
    avatar: state.auth.avatar,
    userName: state.auth.userName,
    isAdmin: state.auth.isAdmin,
  }));

  const [text, setText] = useState('');
  const [message, setMessage] = useState({});

  useEffect(() => {
    if (editMessageId) {
      const editMessage = messages.find(
        (message) => message.id === editMessageId
      );

      setText(editMessage!.text); // eslint-disable-line
      setMessage(editMessage!);
    }
  }, [editMessageId]); // eslint-disable-line

  const handleSave = () => {
    if (text) {
      const user: IUser = {
        userId,
        avatar,
        userName,
        isAdmin,
      };

      dispatch(
        handleUpdateMessage(
          `${apiUrl}/messages/${editMessageId}`,
          editMessageId,
          text,
          user,
          message as IMessage
        )
      );
    }
    dispatch(setEditMode(false));
  };

  const handleCancel = () => {
    setText('');
    dispatch(setEditMessageId(''));
    dispatch(setEditMode(false));
  };

  return (
    <div className={`${styles.wrapper} ${editModal ? 'showModal' : ''}`}>
      <article
        className={`${styles.modal} edit-message-modal ${
          editModal ? 'modal-shown' : ''
        }`}
      >
        <button
          className={`${styles.close} edit-message-close`}
          type="button"
          onClick={handleCancel}
        >
          <img src={closeIcon} alt="Закрыть" />
          <span className="visually-hidden">Закрыть</span>
        </button>
        <textarea
          className={`${styles.textArea} edit-message-input`}
          value={text}
          onChange={(ev: React.FormEvent<HTMLTextAreaElement>) =>
            setText(ev.currentTarget.value)
          }
        />
        <button
          className={`${styles.save} edit-message-button`}
          type="button"
          onClick={handleSave}
        >
          сохранить
        </button>
      </article>
    </div>
  );
};

export default MessageEdit;
