import { combineReducers } from '@reduxjs/toolkit';

import chat from '../state/chat/chat.reducer';
import auth from '../state/auth/auth.reducer';

const rootReducer = combineReducers({
  auth,
  chat,
});

export type IRootState = ReturnType<typeof rootReducer>;

export default rootReducer;
