import db from '../config/db';
import { IMessage, INewUser } from '../services/interfaces';

class BaseRepository{
  db = undefined as any; //eslint-disable-line

  collectionName = '';

  constructor(collectionName:string) {
    this.db = db.getData('/');
    this.collectionName = collectionName;
  }

  getOne(search:string, isUsername:boolean, isUser = false):any { // eslint-disable-line
    const searchParam = isUsername ? 'username' : isUser ? 'userId' :'id'; // eslint-disable-line
    const index = db.getIndex(this.collectionName, search, searchParam);

    if (index !== -1) {
      return db.getData(this.collectionName)[index];
    }

    return false;
  }

  getAll():[] {
    const data = db.getData(this.collectionName);

    if (data) {
      return data;
    }

    return [];
  }

  create(data:any):boolean { //eslint-disable-line
    try {
      const currentData = db.getData(this.collectionName);

      db.push(`${this.collectionName}[${currentData.length}]`, data);
      return true;
    } catch {
      return false;
    }
  }

  update(id:string, body:IMessage | INewUser):boolean {
    try {
      const index = db.getIndex(this.collectionName, id, 'id');
      db.push(`${this.collectionName}[${index}]`, body);
  
      return true;
    } catch {
      return false;
    }
  }

  delete(id:string):string | boolean {
    try {
      const index = db.getIndex(this.collectionName, id, 'id');
      db.delete(`${this.collectionName}[${index}]`);
  
      return id;
    } catch {
      return false;
    }
  }
}

export default BaseRepository;