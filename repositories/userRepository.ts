import BaseRepository from './baseRepository';

class UserRepository extends BaseRepository {
  constructor() {
    super('/users');
  }
}

export default new UserRepository();
