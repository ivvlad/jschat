import BaseRepository from './baseRepository';

class MessagesRepository extends BaseRepository {
  constructor() {
    super('/messages');
  }
}

export default new MessagesRepository();
