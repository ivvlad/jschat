import express from 'express';
import auth from './auth';
import users from './users';
import messages from './messages';

export default (app: express.Application): void => {
  app.use('/api/auth', auth);
  app.use('/api/users', users);
  app.use('/api/messages', messages);
};
