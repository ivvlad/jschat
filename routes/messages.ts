import express, { Router } from 'express';
import messagesService from '../services/messagesService';

const router = Router();

router.get('/', (req: express.Request, res: express.Response) => {
  try {
    messagesService.getMessages(res);
  } catch {
    res.status(500).json({ err: 'Req failed' });
  }
});

router.post('/new', (req: express.Request, res: express.Response) => {
  try {
    messagesService.createMessage(req.body, res);
  } catch {
    res.status(500).json({ err: 'Req failed' });
  }
});

router.put('/:id', (req: express.Request, res: express.Response) => {
  try {
    messagesService.updateMessage(req.params.id, req.body, res);
  } catch {
    res.status(500).json({ err: 'Req failed' });
  }
});

router.delete('/:id', (req: express.Request, res: express.Response) => {
  try {
    messagesService.deleteMessage(req.params.id, res);
  } catch {
    res.status(500).json({ err: 'Req failed' });
  }
});

export default router;
