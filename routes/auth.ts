import express, { Router } from 'express';
import AuthSevice from '../services/authSerivce';

const router = Router();

router.post('/login', (req: express.Request, res: express.Response) => {
  try {
    AuthSevice.login(req.body, res);
  } catch (err) {
    res.status(500).json({ err: 'Req failed' });
  }
});

export default router;
