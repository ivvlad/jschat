import express, { Router } from 'express';
import userService from '../services/userService';

const router = Router();

router.get('/', (req: express.Request, res: express.Response) => {
  try {
    userService.getUsers(res);
  } catch (err) {
    res.status(500).json({ err: 'Req failed' });
  }
});

router.post('/new', (req: express.Request, res: express.Response) => {
  try {
    userService.createUser(req, res);
  } catch (err) {
    res.status(500).json({ err: 'Req failed' });
  }
});

router.put('/:id', (req: express.Request, res: express.Response) => {
  try {
    userService.updateUser(req.params.id, req.body, res);
  } catch {
    res.status(500).json({ err: 'Req failed' });
  }
});

router.delete('/:id', (req: express.Request, res: express.Response) => {
  try {
    userService.deleteUser(req.params.id, res);
  } catch {
    res.status(500).json({ err: 'Req failed' });
  }
});

export default router;
